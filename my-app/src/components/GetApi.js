import React, { useState } from 'react';
import axios from 'axios';
const GetApi = () => {

    const [posts, setPosts] = useState([{
        "title": "",
        "body": ""
    }])
    async function fetchPosts() {
    
            const request = await axios.get("https://jsonplaceholder.typicode.com/posts");
            const Data = request.data;
            setPosts(Data);
        
    }

    return (
        <div>
            <h1>GET REQUEST</h1>
            <button onClick={fetchPosts}>BUTTON</button>
            {
                posts.map((Posts) => {
                    return (
                        <>
                            <p>{Posts.title}</p>
                            <p>{Posts.body}</p>
                        </>
                    )
                })
            }
        </div>
    );
}
export default GetApi;