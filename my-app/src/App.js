
import './App.css';
import Post from './components/PostApi';
import Get from './components/GetApi.js';



function App() {
  return (
   <div className = "App">
     <Post />
     <Get />
   </div>
  );
}

export default App;
